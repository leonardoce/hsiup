colorize Nimrod {
    SyntaxParser        = 'SIMPLE';
    
    color {
        { 'Normal',      'Editor_Default' },
        { 'Number',      'Lang_Number' },
        { 'Punctuation', 'Lang_Punctuation' },
        { 'String',      'Lang_String' },
        { 'Comment',     'Lang_Comment' },
        { 'Function',    'Lang_Function' },
    };
    
    keyword 'Editor_Keywords' {
            'addr',
            'and',
            'as',
            'asm',
            'atomic',
            'bind',
            'block',
            'break',
            'case',
            'cast',
            'const',
            'continue',
            'converter',
            'discard',
            'distinct',
            'div',
            'elif',
            'else',
            'end',
            'enum',
            'except',
            'export',
            'finally',
            'for',
            'from',
            'generic',
            'if',
            'import',
            'in',
            'include',
            'is',
            'isnot',
            'iterator',
            'lambda',
            'let',
            'macro',
            'method',
            'mod',
            'nil',
            'not',
            'notin',
            'object',
            'of',
            'or',
            'out',
            'proc',
            'ptr',
            'raise',
            'ref',
            'return',
            'shl',
            'shr',
            'template',
            'try',
            'tuple',
            'type',
            'var',
            'when',
            'while',
            'with',
            'without',
            'xor',
            'yield',
    }; 
    h_state 0 { 'Normal' }
    h_trans { 4, '-s', 'a-zA-Z_', 'Normal' }
    h_trans { 1, '', '#', 'Comment' }
    h_trans { 6, "<i", `"""`, 'String' }
    h_trans { 2, '', '"', 'String' }
    h_trans { 3, '', '\'', 'String' }
    h_trans { 5, 's', '0-9', 'Number' }
    h_trans { 0, 'S', '_a-zA-Z0-9', 'Punctuation' }

    h_state 1 { 'Comment' }
    h_trans { 0, '$', '', 'Normal' }

    h_state 2 { 'String' }
    h_trans { 0, '', '"', 'String' }
    h_trans { 0, '$', '', 'String' }
    h_trans { 4, 'Qq', '\\', 'String' }

    h_state 3 { 'String' }
    h_trans { 0, '', '\'', 'String' }
    h_trans { 0, '$', '', 'String' }

    h_state 4 { 'Normal' }
    h_trans { 0, '$', '', 'Normal' }
    h_wtype { 0, 0, 0, '', 'a-zA-Z0-9_' }

    h_state 5 { 'Number' }
    h_trans { 0, '-S', '0-9', 'Normal' }
    h_trans { 0, '$', '', 'Normal' }

    h_state 6 { 'String' }
    h_trans { 6, ">", "\\\\", "String" }
    h_trans { 0, ">", `"""`, "String" }
    h_trans { 6, ">", `\\"`, "String" }
}

mode Nimrod: SOURCE {               # Haskell mode
    FileNameRx          = '\\.\\c{nim}$';
    Colorizer           = 'Nimrod';
    HilitOn             = 1;
    AutoIndent          = 1;
    IndentMode          = 'PLAIN';
    MatchCase           = 1;
    Trim                = 1;
    MultiLineHilit      = 0;
    AutoHilitParen      = 1;
    
    # RoutineRegexp       = /^\s*{function}|{procedure}\s/;
    
    SaveFolds           = 2;      # save fold info at end of line
    CommentStart        = '# ';
    CommentEnd          = '';
}

oinclude 'mym_haskell.fte';
