-- | 
-- Module: Graphics.UI.IUP
-- Bundles all IUP library functionalities

module Graphics.UI.IUP 
	(
	module Graphics.UI.IUP.Externals,
	module Graphics.UI.IUP.Attributes,
	module Graphics.UI.IUP.Helpers,
	) where

import Graphics.UI.IUP.Externals
import Graphics.UI.IUP.Attributes
import Graphics.UI.IUP.Helpers
