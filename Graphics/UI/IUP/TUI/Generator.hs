module Graphics.UI.IUP.TUI.Generator
  (
  TUIGrid(..),
  createBoxFromTUI,
  ) where

import Graphics.UI.IUP.TUI.Parser
import Graphics.UI.IUP.Externals
import Graphics.UI.IUP.Attributes

data TUIGrid = TUIGrid {
  cellWidth :: Int,
  cellHeight :: Int
}

type IUPAssoc = [(String, Handle)]

-- Create IUP component from TUI description
createFromTUI :: TUIGrid -> TUIComponent -> IO Handle

createFromTUI grid comp@TUIComponent {componentType=Label, position=(x,y)} = do
  comp <- iupLabel (text comp)
  set comp [cx := ((cellWidth grid) * x), cy := ((cellHeight grid) * y)]
  return comp

createFromTUI grid comp@TUIComponent {componentType=TextField, position=(x,y),
    Graphics.UI.IUP.TUI.Parser.size=(sx,sy)} = do
  comp <- iupText
  set comp [
    cx := (cellWidth grid * x),
    cy := (cellHeight grid * y),
    rastersize := (cellWidth grid * sx, cellHeight grid*sy)
    ]
  return comp

createFromTUI grid comp@TUIComponent {componentType=Button, position=(x,y),
    Graphics.UI.IUP.TUI.Parser.size=(sx,sy), text=text} = do
  comp <- iupButton text
  set comp [
    cx := (cellWidth grid * x),
    cy := (cellHeight grid * y),
    rastersize := (cellWidth grid * sx, cellHeight grid*sy)
    ]
  return comp

createFromTUI grid comp@TUIComponent {componentType=ToggleButton, position=(x,y),
    Graphics.UI.IUP.TUI.Parser.size=(sx,sy), text=text} = do
  comp <- iupToggle text
  set comp [
    cx := (cellWidth grid * x),
    cy := (cellHeight grid * y),
    rastersize := (cellWidth grid * sx, cellHeight grid*sy)
    ]
  return comp

-- | This function will create an IUP box from a component
-- TUI description
createBoxFromTUI :: TUIGrid -> [TUIComponent] -> IO (Handle, IUPAssoc)
createBoxFromTUI grid description = do
  box <- iupCbox []
  assoc <- createAdd box description []
  return (box, assoc)
  where
    createAdd :: Handle -> [TUIComponent] -> IUPAssoc -> IO IUPAssoc
    createAdd box [] _ = return []
    createAdd box (component:others) assocList = do
      handle <- createFromTUI grid component
      iupAppend box handle
      assocList <- createAdd box others ((name component, handle):assocList)
      return assocList
