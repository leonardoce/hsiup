module Graphics.UI.IUP.TUI.Parser
  (
  ComponentType(..),
  TUIComponent(..),
  tuiParser,
  ) where

import Data.List
import Graphics.UI.IUP.Utils

-- Textual UI definition
--
-- Every label ends with two consecutive spaces

data ComponentType = TextField | Button | TextArea | Label | ToggleButton
  deriving Show

data TUIComponent = TUIComponent {
  name :: String,
  text :: String,
  position :: (Int,Int),
  size :: (Int,Int),
  componentType :: ComponentType
} deriving Show

type TUIDescription = [String]

-- | Parse a TUI description
tuiParser :: TUIDescription -> [TUIComponent]
tuiParser description = tuiParserInternal description 0

-- tuiParserInternal <description> <currentRow> -> <componentList>
tuiParserInternal :: TUIDescription -> Int -> [TUIComponent]
tuiParserInternal [] _ = []
tuiParserInternal (x:xs) currentY = (tuiRowParser x currentY) ++ other
  where other = tuiParserInternal xs (currentY + 1)

tuiRowParser :: String -> Int -> [TUIComponent]
tuiRowParser descriptionRow currentRow = tuiRowParserInternal descriptionRow currentRow 0

tuiRowParserInternal :: String -> Int -> Int -> [TUIComponent]
tuiRowParserInternal "" _ _ = []
tuiRowParserInternal descriptionRow currentRow currentColumn
  -- Skip spaces
  | head descriptionRow == ' ' = tuiRowParserInternal (tail descriptionRow)
                                 currentRow (currentColumn+1)

  -- Parse text area
  | head descriptionRow == '|' =
    case findSubStr "|" (tail descriptionRow) of
      Just idx -> [mkTextArea currentRow currentColumn (take (idx + 2) descriptionRow)]
        ++ tuiRowParserInternal (drop (idx+2) descriptionRow) currentRow (currentColumn + idx + 2)
      -- This is a syntax error
      Nothing -> []

  -- Parse text fields
  | head descriptionRow == '[' =
    case findSubStr "]" descriptionRow of
      Just idx -> [mkTextField currentRow currentColumn (take (idx+1) descriptionRow)]
        ++ tuiRowParserInternal (drop (idx+1) descriptionRow) currentRow (currentColumn + idx +1)
      -- This is a syntax error
      Nothing -> []

  -- Parse buttons
  | isPrefixOf "(bt" descriptionRow =
    case findSubStr ")" descriptionRow of
      Just idx -> [mkButton currentRow currentColumn (take (idx+1) descriptionRow)]
        ++ tuiRowParserInternal (drop (idx+1) descriptionRow) currentRow (currentColumn + idx +1)
      -- This is a syntax error
      Nothing -> []

  -- Parse toggle
  | isPrefixOf "(cb" descriptionRow =
    case findSubStr ")" descriptionRow of
      Just idx -> [mkToggleButton currentRow currentColumn (take (idx+1) descriptionRow)]
        ++ tuiRowParserInternal (drop (idx+1) descriptionRow) currentRow (currentColumn + idx +1)
      -- This is a syntax error
      Nothing -> []

  -- Parse labels
  | otherwise =
    case findSubStr "  " descriptionRow of
      Just idx -> [mkLabel currentRow currentColumn (take idx descriptionRow)]
        ++ tuiRowParserInternal (drop (idx) descriptionRow) currentRow (currentColumn + idx)
      Nothing -> [mkLabel currentRow currentColumn descriptionRow]

-- Component creation functions
mkLabel :: Int -> Int -> String -> TUIComponent
mkLabel row col text = TUIComponent {
  name = "",
  text=text,
  position=(col, row),
  size=(0,0),
  componentType = Label
  }

mkGenericComponent :: ComponentType -> Int -> Int -> String -> TUIComponent
mkGenericComponent componentType row col text = TUIComponent {
  name = getComponentName text,
  text = text,
  position = (col, row),
  size = (length text, 1),
  componentType = componentType
  }

mkButton :: Int -> Int -> String -> TUIComponent
mkButton = mkGenericComponent Button

mkToggleButton :: Int -> Int -> String -> TUIComponent
mkToggleButton = mkGenericComponent ToggleButton

mkTextArea :: Int -> Int -> String -> TUIComponent
mkTextArea = mkGenericComponent TextArea

mkTextField :: Int -> Int -> String -> TUIComponent
mkTextField = mkGenericComponent TextField

getComponentName :: String -> String
getComponentName txt = txtWithoutCarets
  where
  txtWithoutBrackets = reverse $ drop 1 $ reverse $ drop 1 txt
  txtWithoutCarets = reverse $ dropSeps $ reverse $ dropSeps txtWithoutBrackets

  dropSeps :: String -> String
  dropSeps "" = ""
  dropSeps ('-':xs) = dropSeps xs
  dropSeps (' ':xs) = dropSeps xs
  dropSeps s = s
