{-# LANGUAGE ExistentialQuantification #-}
-- | IUP Helper functions

module Graphics.UI.IUP.Attributes (
  -- * Helpers
  set,
  AttributeOP(..),
  Color,
  -- * Attributes
  title,
  action,
  cx,
  cy,
  size,
  rastersize,
  margin,
  cmargin,
  bgcolor,
  fgcolor,
  background,
  ) where

import Graphics.UI.IUP.Externals
import Graphics.UI.IUP.Utils
import Data.String.Utils

data Attribute a = Attribute { setter :: Handle -> a -> IO (), getter :: Handle -> IO a }

-- | Construct an attribute table row
data AttributeOP = forall a . (Attribute a) := a 

-- | Set attributes on a IUP component handle
set :: Handle -> [AttributeOP] -> IO ()
set _ [] = return ()
set handle ((attr := value):xs) = (setter attr) handle value >> (set handle xs)

-- | The RGB components.
type Color = (Int, Int, Int)

---------------------
-- Attribute types --
---------------------

stringAttribute :: String -> Attribute String
stringAttribute name = Attribute { setter=setter, getter=getter }
  where
    getter handle = iupGetAttribute handle name
    setter handle value = iupSetAttribute handle name value

intAttribute :: String -> Attribute Int
intAttribute name = Attribute { setter=setter, getter=getter}
  where
    setter handle value = iupSetAttribute handle name (show value)
    getter handle = iupGetAttribute handle name >>= \ x -> return (read x)

sizeAttribute :: String -> Attribute (Int, Int)
sizeAttribute name = Attribute { setter=setter, getter=getter }
  where
    setter handle value@(x,y) = iupSetAttribute handle name (show x ++ "x" ++ show y)
    getter handle = iupGetAttribute handle name >>= \ x -> return (parseSize x)

    parseSize :: String -> (Int, Int)
    parseSize value = case findSubStr "x" value of
      Nothing -> (0,0)
      Just x ->
        let primo = take x value in
        let secondo = drop (x+1) value in
        (read primo, read secondo)

colorAttribute :: String -> Attribute Color
colorAttribute name = Attribute {setter=setter, getter=getter}
  where
    setter handle value@(r,g,b) = iupSetAttribute handle name (show r ++ " " ++ show g ++ " " ++ show b)
    getter handle = iupGetAttribute handle name >>= \ x -> return (parseColor x)

    parseColor :: String -> (Int, Int, Int)
    parseColor value =  case split " " value of
      [r,g,b] -> (read r, read g, read b)
      _ -> (0,0,0)

----------------
-- Attributes --
----------------

action :: Attribute Callback
action = Attribute {setter=setter, getter=getter}
  where
    setter handle value = iupSetCallback handle "ACTION" value
    getter handle = error "Could not get callback"

title :: Attribute String
title = stringAttribute "TITLE"

cx :: Attribute Int
cx = intAttribute "CX"

cy :: Attribute Int
cy = intAttribute "CY"

size :: Attribute (Int, Int)
size = sizeAttribute "SIZE"

rastersize :: Attribute (Int, Int)
rastersize = sizeAttribute "RASTERSIZE"

margin :: Attribute (Int, Int)
margin = sizeAttribute "MARGIN"

cmargin :: Attribute (Int, Int)
cmargin = sizeAttribute "CMARGIN"

bgcolor :: Attribute Color
bgcolor = colorAttribute "BGCOLOR"

fgcolor :: Attribute Color
fgcolor = colorAttribute "FGCOLOR"

background :: Attribute Color
background = colorAttribute "BACKGROUND"
