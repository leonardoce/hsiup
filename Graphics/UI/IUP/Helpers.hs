module Graphics.UI.IUP.Helpers 
	(
	withIUP
	) where

import Graphics.UI.IUP.Externals

-- | Initialize and finalize IUP Library
withIUP :: IO a -> IO a
withIUP action = do
  iupOpen
  a <- action
  iupClose
  return a
