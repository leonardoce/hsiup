import Graphics.UI.IUP
import Graphics.UI.IUP.TUI

-- Little test for TUI --FOLD00
graphicalInterface :: [String]
graphicalInterface = [
  "Cognome:       [------------------------------]",
  "",
  "Nome:          [------------------------------]",
  "",
  "Indirizzo:     [------------------------------]",
  "               [------------------------------]",
  "               [------------------------------]",
  "",
  "Via:  [--------------------------]  n.c.  [---]",
  "",
  "Attivo:        (cbAttivo)",
  "",
  "(btOK) (btAnnulla)"
  ]

-- Little test for HsIUP functions --FOLD00

buttonCallback handle = do
  putStrLn "This is the button callback!"
  return Close

main = withIUP $ do
  v <- iupVersion
  putStrLn ("I'm running on Iup version " ++ v)

  (box, _) <- createBoxFromTUI (TUIGrid {cellWidth=6, cellHeight=20}) (tuiParser graphicalInterface)
  scatola <- iupVbox [box]
  set scatola [ margin := (40,40) ]
  dialog <- iupDialog scatola
  set dialog [ background := (200,200,200) ]

  set dialog [ title := "This is the dialog title" ]

  iupShow dialog
  iupMainLoop
