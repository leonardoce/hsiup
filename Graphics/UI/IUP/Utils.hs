module Graphics.UI.IUP.Utils
  (
  findSubStr,
  ) where

import Data.List

-- | String utils
findSubStr :: String -> String -> Maybe Int
findSubStr needle str = findIndex (needle `isPrefixOf`) (tails str)