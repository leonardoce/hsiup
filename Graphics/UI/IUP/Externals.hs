{-# LANGUAGE ForeignFunctionInterface #-}

-- |
-- This module bind the IUP external functions

module Graphics.UI.IUP.Externals (
  -- * Types
  Handle,
  Callback,
  CallbackResult(..),
  -- * Functions
  -- ** System
  iupOpen,
  iupClose,
  iupVersion,
  iupLoad,
  iupLoadBuffer,
  -- ** Attributes
  iupSetAttribute,
  iupResetAttribute,
  iupGetAttribute,
  iupSetGlobal,
  iupGetGlobal,
  -- ** Events
  iupMainLoop,
  iupMainLoopLevel,
  iupLoopStep,
  iupExitLoop,
  iupFlush,
  iupSetCallback,
  -- ** Layout
  -- *** Composition
  iupFill,
  iupHbox,
  iupVbox,
  iupZbox,
  iupRadio,
  iupNormalizer,
  iupCbox,
  iupSbox,
  iupSplit,
  -- *** Hierarchy
  iupAppend,
  -- ** Dialogs
  -- *** Reference
  iupDialog,
  iupPopup,
  iupShow,
  iupShowXY,
  iupHide,
  -- *** Predefined
  -- ** Controls
  iupButton,
  iupFrame,
  iupLabel,
  iupList,
  iupProgressBar,
  iupText,
  iupToggle,
) where

import Foreign.Ptr;
import Foreign.C.String;
import Foreign.Marshal;

-----------
-- Types --
-----------

data HandlePriv = HandlePriv
type Handle = Ptr HandlePriv
type Callback = Handle -> IO CallbackResult

data CallbackResult = Ignore | Default | Close | Continue

------------
-- System --
------------

foreign import ccall unsafe "IupOpen" c_iupOpen :: IntPtr -> IntPtr -> IO ()

-- | Initializes the IUP toolkit. Must be called before any other IUP function.
iupOpen :: IO ()
iupOpen = c_iupOpen 0 0

-- | Ends the IUP toolkit and releases internal memory. 
-- It will also automatically destroy all dialogs and all elements that have names.
foreign import ccall unsafe "IupClose" iupClose :: IO ()

foreign import ccall unsafe "IupVersion" c_iupVersion :: IO CString

-- | Returns a string with the IUP version number.
iupVersion :: IO String
iupVersion = c_iupVersion >>= peekCString

foreign import ccall unsafe "IupLoad" c_iupLoad :: CString -> IO CString
foreign import ccall unsafe "IupLoadBuffer" c_iupLoadBuffer :: CString -> IO CString

-- | Compiles a LED specification.
iupLoad :: String -> IO String
iupLoad fileName = withCString fileName c_iupLoad >>= peekCString
 
-- | Compiles a LED specification.
iupLoadBuffer :: String -> IO String
iupLoadBuffer buffer = withCString buffer c_iupLoadBuffer >>= peekCString


----------------
-- Attributes --
----------------

foreign import ccall unsafe "IupStoreAttribute" c_iupSetAttribute :: Handle -> CString -> CString -> IO ()

-- | Defines an attribute for an interface element. See also the Attributes Guide section.
iupSetAttribute :: Handle -> String -> String -> IO ()
iupSetAttribute handle name value = 
  withCString name $ \ c_name ->
  withCString value $ \ c_value ->
  c_iupSetAttribute handle c_name c_value 


foreign import ccall unsafe "IupResetAttribute" c_iupResetAttribute :: Handle -> CString -> IO ()

-- | Removes an attribute from the hash table of the element, and its children if the 
-- attribute is inheritable. It is useful to reset the state of inheritable 
-- attributes in a tree of elements.
iupResetAttribute :: Handle -> String -> IO ()
iupResetAttribute handle name = withCString name $ \ c_name -> c_iupResetAttribute handle c_name

foreign import ccall unsafe "IupGetAttribute" c_iupGetAttribute :: Handle -> CString -> IO CString

-- | Returns the name of an interface element attribute. See also the Attributes Guide section.
iupGetAttribute :: Handle -> String -> IO String
iupGetAttribute handle name = withCString name $ \ c_name ->
  c_iupGetAttribute handle c_name >>= peekCString 

foreign import ccall unsafe "IupSetGlobal" c_iupStoreGlobal :: CString -> CString -> IO ()

-- | Defines an attribute for the global environment but the string is internally duplicated. 
iupSetGlobal :: String -> String -> IO ()
iupSetGlobal name value = 
  withCString name $ \ c_name ->
  withCString value $ \ c_value ->
  c_iupStoreGlobal c_name c_value

foreign import ccall unsafe "IupGetGlobal" c_iupGetGlobal :: CString -> IO CString

-- | Returns an attribute value from the global environment. The value can be 
-- returned from the driver or from the internal storage.
iupGetGlobal :: String -> IO String
iupGetGlobal name = withCString name c_iupGetGlobal >>= peekCString

------------
-- Events --
------------

-- | Executes the user interaction until a callback returns IUP_CLOSE, IupExitLoop is 
-- called, or hiding the last visible dialog.
foreign import ccall safe "IupMainLoop" iupMainLoop :: IO ()

-- | Returns the current cascade level of IupMainLoop. When no calls were done, return 
-- value is 0.
foreign import ccall unsafe "IupMainLoopLevel" iupMainLoopLevel :: IO Int

-- | Runs one iteration of the message loop.
foreign import ccall safe "IupLoopStep" iupLoopStep :: IO Int

-- | Terminates the current message loop. It has the same effect of a callback returning 
-- IUP_CLOSE.
foreign import ccall unsafe "IupExitLoop" iupExitLoop :: IO ()

-- | Processes all pending messages in the message queue.
foreign import ccall unsafe "IupFlush" iupFlush :: IO ()

foreign import ccall "wrapper" mkExternalCallback :: (Handle -> IO Int) -> IO (FunPtr (Handle -> IO Int)) 
foreign import ccall unsafe "IupSetCallback" c_iupSetCallback :: Handle -> CString -> FunPtr (Handle -> IO Int) -> IO ()

callbackResultToInteger Ignore = (-1)
callbackResultToInteger Default = (-2)
callbackResultToInteger Close = (-3)
callbackResultToInteger Continue = (-4)

iupSetCallback :: Handle -> String -> Callback -> IO ()
iupSetCallback handle name callback = withCString name $ \ c_name -> do
  ck <- mkExternalCallback callbackExternal
  c_iupSetCallback handle c_name ck
  where
    callbackExternal :: Handle -> IO Int
    callbackExternal handle = do
      value <- callback handle
      return (callbackResultToInteger value)
  
------------
-- Layout --
------------

-- | Creates a Fill composition element, which dynamically occupies empty spaces 
-- always trying to expand itself. Its parent must be an IupHbox or an IupVbox. 
-- If an EXPAND is set on at least one of the other children of the box, then 
-- the Fill is ignored.
foreign import ccall unsafe "IupFill" iupFill :: IO Handle

foreign import ccall unsafe "IupHboxv" c_iupHboxv :: Ptr Handle -> IO Handle

-- | Creates a void container for composing elements horizontally. It is a box 
-- that arranges the elements it contains from left to right.
iupHbox :: [Handle] -> IO Handle
iupHbox handleList = withArray0 nullPtr handleList c_iupHboxv

foreign import ccall unsafe "IupVboxv" c_iupVboxv :: Ptr Handle -> IO Handle

-- | Creates a void container for composing elements vertically. It is a box that 
-- arranges the elements it contains from top to bottom.
iupVbox :: [Handle] -> IO Handle
iupVbox handleList = withArray0 nullPtr handleList c_iupVboxv

foreign import ccall unsafe "IupZboxv" c_iupZboxv :: Ptr Handle -> IO Handle

-- | Creates a void container for composing elements in hidden layers with only 
-- one layer visible. It is a box that piles up the children it contains, only 
-- the one child is visible.
iupZbox :: [Handle] -> IO Handle
iupZbox handleList = withArray0 nullPtr handleList c_iupZboxv

-- | Creates a void container for grouping mutual exclusive toggles. Only one 
-- of its descendent toggles will be active at a time. The toggles can be at 
-- any composition.
foreign import ccall unsafe "IupRadio" iupRadio :: Handle -> IO Handle

foreign import ccall unsafe "IupNormalizerv" c_iupNormalizer :: Ptr Handle -> IO Handle

-- | Normalizes all controls from a list so their natural size to be the biggest 
-- natural size among them. All natural width will be set to the biggest width, 
-- and all natural height will be set to the biggest height according to is value. 
iupNormalizer :: [Handle] -> IO Handle
iupNormalizer handleList = withArray0 nullPtr handleList c_iupNormalizer

foreign import ccall unsafe "IupCboxv" c_iupCboxv :: Ptr Handle -> IO Handle

-- | Creates a void container for position elements in absolute coordinates. 
-- It is a concrete layout container.
iupCbox :: [Handle] -> IO Handle
iupCbox handleList = withArray0 nullPtr handleList c_iupCboxv

-- | Creates a void container that allows its child to be resized. Allows 
-- expanding and contracting the child size in one direction.
foreign import ccall unsafe "IupSbox" iupSbox :: Handle -> IO Handle

-- | Creates a void container that split its client area in two. Allows the provided 
-- controls to be enclosed in a box that allows expanding and contracting the element 
-- size in one direction, but when one is expanded the other is contracted.
foreign import ccall unsafe "IupSplit" iupSplit :: Handle -> Handle -> IO Handle

foreign import ccall unsafe "IupAppend" iupAppend :: Handle -> Handle -> IO Handle

-------------
-- Dialogs --
-------------

-- | Creates a dialog element. It manages user interaction with the interface 
-- elements. For any interface element to be shown, it must be encapsulated in a dialog.
foreign import ccall unsafe "IupDialog" iupDialog :: Handle -> IO Handle

-- | Shows a dialog or menu and restricts user interaction only to the specified 
-- element. It is equivalent of creating a Modal dialog is some toolkits.
--
-- If another dialog is shown after IupPopup using IupShow, then its interaction 
-- will not be inhibited. Every IupPopup call creates a new popup level that inhibits 
-- all previous dialogs interactions, but does not disable new ones. 
--
-- IMPORTANT: The popup levels must be closed in the reverse order they were 
-- created or unpredictable results will occur.
--
-- For a dialog this function will only return the control to the application after 
-- a callback returns IUP_CLOSE, IupExitLoop is called, or when the popup dialog is 
-- hidden, for example using IupHide. For a menu it returns automatically after a 
-- menu item is selected. IMPORTANT: If a menu item callback returns IUP_CLOSE, 
-- it will ends the current popup level dialog. 
foreign import ccall unsafe "IupPopup" iupPopup :: Handle -> Int -> Int -> IO ()

-- | Displays a dialog in the current position, or changes a control VISIBLE attribute. 
-- If the dialog needs to be mapped and the current position is not known then the 
-- dialog is centered. 
foreign import ccall unsafe "IupShow" iupShow :: Handle -> IO ()

-- | Displays a dialog in a given position on the screen.
foreign import ccall unsafe "IupShowXY" iupShowXY :: Handle -> Int -> Int -> IO ()

-- | Hides an interface element. This function has the same effect as attributing 
-- value "NO" to the interface element’s VISIBLE attribute.
foreign import ccall unsafe "IupHide" iupHide :: Handle -> IO ()


----------------
-- Components --
----------------

foreign import ccall unsafe "IupButton" c_iupButton :: CString -> CString -> IO Handle

-- | Creates an interface element that is a button. When selected, this element 
-- activates a function in the application. Its visual presentation can contain 
-- a text and/or an image.
iupButton :: String -> IO Handle
iupButton title = withCString title $ \ c_title -> c_iupButton c_title nullPtr

-- | Creates a Frame interface element, which draws a frame with a title around an
-- interface element.
foreign import ccall unsafe "IupFrame" iupFrame :: Handle -> IO Handle

foreign import ccall unsafe "IupLabel" c_iupLabel :: CString -> IO Handle

-- | Creates a label interface element, which displays a separator, a text or an
-- image.
iupLabel :: String -> IO Handle
iupLabel title = withCString title c_iupLabel

foreign import ccall unsafe "IupList" c_iupList :: CString -> IO Handle

-- | Creates an interface element that displays a list of items. The list can be
-- visible or can be dropped down. It also can have an edit box for text input.
-- So it is a 4 in 1 element. In native systems the dropped down case is called
-- Combo Box.
iupList :: IO Handle
iupList = c_iupList nullPtr

-- | Creates a progress bar control. Shows a percent value that can be updated to
-- simulate a progression.
foreign import ccall unsafe "IupProgressBar" iupProgressBar :: IO Handle

foreign import ccall unsafe "IupText" c_iupText :: CString -> IO Handle

-- | Creates an editable text field.
iupText :: IO Handle
iupText = c_iupText nullPtr

foreign import ccall unsafe "IupToggle" c_iupToggle :: CString -> IO Handle

-- | Creates the toggle interface element. It is a two-state (on/off) button that,
-- when selected, generates an action that activates a function in the associated
-- application. Its visual representation can contain a text or an image.
iupToggle :: String -> IO Handle
iupToggle title = withCString title c_iupToggle
